// Is called when the page is first loaded (or possibly reloaded). If this is
//   a new load, then it sets up variables. If this is a reload (old user) then
//   it does nothing. If the browser doesn't support local storage then it
//   displays a message to the user asking them to update their browser.

var scores = {
  "A1":{
    "Matter":30,
    "Method":15,
    "Manner":30,
    "Total":75
  },"A2":{
    "Matter":30,
    "Method":15,
    "Manner":30,
    "Total":75
  },"A3":{
    "Matter":30,
    "Method":15,
    "Manner":30,
    "Total":75
  },"N1":{
    "Matter":30,
    "Method":15,
    "Manner":30,
    "Total":75
  },"N2":{
    "Matter":30,
    "Method":15,
    "Manner":30,
    "Total":75
  },"N3":{
    "Matter":30,
    "Method":15,
    "Manner":30,
    "Total":75
  },
  "Aff":255,
  "Neg":255
}

window.onLoad = init();
function init(){
  screen(0);
}

// This will hide all <main> elements (each one is it's own seperate screen) and
//   then make visible the selected screen.
function screen(screen){
  var elements = document.getElementsByTagName('main');
  for(var i = 0; i < elements.length; i++){
    elements[i].style.display = 'none';
  }
  switch(screen){
  case 0:
    document.getElementById('speakers').style.display = 'block';
    break;
  case 1:
    document.getElementById('scores').style.display = 'block';
    updateScores();
    break;
  case 2:
    document.getElementById('about').style.display = 'block';
    break;
  }
}

// When called this will update the table with the scores with the data thats
//   currently in sessionStorage.
function updateScores(){
  scores.A1.Total = scores.A1.Matter + scores.A1.Method + scores.A1.Manner;
  scores.A2.Total = scores.A2.Matter + scores.A2.Method + scores.A2.Manner;
  scores.A3.Total = scores.A3.Matter + scores.A3.Method + scores.A3.Manner;
  scores.N1.Total = scores.N1.Matter + scores.N1.Method + scores.N1.Manner;
  scores.N2.Total = scores.N2.Matter + scores.N2.Method + scores.N2.Manner;
  scores.N3.Total = scores.N3.Matter + scores.N3.Method + scores.N3.Manner;
  scores.Aff = scores.A1.Total + scores.A2.Total + scores.A3.Total;
  scores.Neg = scores.N1.Total + scores.N2.Total + scores.N3.Total;
  document.getElementById('A1t').innerHTML = scores.A1.Matter;
  document.getElementById('A1h').innerHTML = scores.A1.Method;
  document.getElementById('A1n').innerHTML = scores.A1.Manner;
  document.getElementById('A1T').innerHTML = scores.A1.Total;
  document.getElementById('A2t').innerHTML = scores.A2.Matter;
  document.getElementById('A2h').innerHTML = scores.A2.Method;
  document.getElementById('A2n').innerHTML = scores.A2.Manner;
  document.getElementById('A2T').innerHTML = scores.A2.Total;
  document.getElementById('A3t').innerHTML = scores.A3.Matter;
  document.getElementById('A3h').innerHTML = scores.A3.Method;
  document.getElementById('A3n').innerHTML = scores.A3.Manner;
  document.getElementById('A3T').innerHTML = scores.A3.Total;
  document.getElementById('ATt').innerHTML = scores.A1.Matter + scores.A2.Matter + scores.A3.Matter;
  document.getElementById('ATh').innerHTML = scores.A1.Method + scores.A2.Method + scores.A3.Method;
  document.getElementById('ATn').innerHTML = scores.A1.Manner + scores.A2.Manner + scores.A3.Manner;
  document.getElementById('ATT').innerHTML = scores.Aff;
  document.getElementById('N1t').innerHTML = scores.N1.Matter;
  document.getElementById('N1h').innerHTML = scores.N1.Method;
  document.getElementById('N1n').innerHTML = scores.N1.Manner;
  document.getElementById('N1T').innerHTML = scores.N1.Total;
  document.getElementById('N2t').innerHTML = scores.N2.Matter;
  document.getElementById('N2h').innerHTML = scores.N2.Method;
  document.getElementById('N2n').innerHTML = scores.N2.Manner;
  document.getElementById('N2T').innerHTML = scores.N2.Total;
  document.getElementById('N3t').innerHTML = scores.N3.Matter;
  document.getElementById('N3h').innerHTML = scores.N3.Method;
  document.getElementById('N3n').innerHTML = scores.N3.Manner;
  document.getElementById('N3T').innerHTML = scores.N3.Total;
  document.getElementById('NTt').innerHTML = scores.N1.Matter + scores.N2.Matter + scores.N3.Matter;
  document.getElementById('NTh').innerHTML = scores.N1.Method + scores.N2.Method + scores.N3.Method;
  document.getElementById('NTn').innerHTML = scores.N1.Manner + scores.N2.Manner + scores.N3.Manner;
  document.getElementById('NTT').innerHTML = scores.Neg;
  var result = scores.Aff - scores.Neg
  updateMessage(result);
  if(Math.abs(result) > 15){
    updateValidation(-1);
  } else if (result == 0){
    updateValidation(-1);
  } else {
    updateValidation(0);
  }
  const totals = ["A1","A2","A3","N1","N2","N3"];
  totals.forEach(function(item,index){
    if(scores[item].Total > 80 ||  scores[item].Total < 70){
      updateValidation(index + 2);
    };
  })
}

// This is called when any button is pressed. It first checks for validity
//   and then, if valid, updates the respective score.
function score(key, mmm, add){
  var tempScore = scores[key][mmm];
  if(add){
    tempScore++;
  } else {
    tempScore--;
  }
  if(mmm == "Method"){
    var newScore = checkValidity(tempScore, true);
  } else {
    var newScore = checkValidity(tempScore, false);
  }
  scores[key][mmm] = newScore
  compositeKey = key + "-" + mmm;
  updateArrow(compositeKey,newScore);
}

// Takes a score and works out if it is within the maximum allowable range
function checkValidity(score, isMethod){
  if(isMethod){
    if(score > 17){
      score = 17;
    } else if (score < 13){
      score = 13;
    }
    return score;
  } else {
    if(score > 34){
      score = 34;
    } else if (score < 26){
      score = 26;
    }
    return score;
  }
}

// This updates the arrow images (well, SVGs really, but whatever) that show
//   the speaker scores.
function updateArrow(key,score){
  switch(score){
    case 34:
      var temp = "img/score_4_more.svg";
      break;
    case 33:
      var temp = "img/score_3_more.svg";
      break;
    case 32:
      var temp = "img/score_2_more.svg";
      break;
    case 31:
      var temp = "img/score_1_more.svg";
      break;
    case 30:
      var temp = "img/score_0_none.svg";
      break;
    case 29:
      var temp = "img/score_1_less.svg";
      break;
    case 28:
      var temp = "img/score_2_less.svg";
      break;
    case 27:
      var temp = "img/score_3_less.svg";
      break;
    case 26:
      var temp = "img/score_4_less.svg";
      break;
    case 17:
      var temp = "img/score_2_more.svg";
      break;
    case 16:
      var temp = "img/score_1_more.svg";
      break;
    case 15:
      var temp = "img/score_0_none.svg";
      break;
    case 14:
      var temp = "img/score_1_less.svg";
      break;
    case 13:
      var temp = "img/score_2_less.svg";
      break;
  }
  document.getElementById(key).src=temp
}

function updateMessage(score){
  if(Math.abs(score) > 15){
    var message = "That winning margin is outside the allowable range!"
  } else if (score == 0){
    var message = "This is not a valid result: It is a draw!";
  } else {
    var message = "Won by the ";
    if(score > 0){
      message += "Affirmative team by a margin of " + score;
    } else {
      message += "Negative team by a margin of " + Math.abs(score);
    }
    if(Math.abs(score) == 1){
      message += " point.";
    } else {
      message += " points.";
    }
  }
  document.getElementById('result').innerHTML = message;
}

function updateValidation(row){
  switch(row){
    case -1:
      document.getElementsByClassName("is-size-7-mobile")[0].classList.add("has-background-danger");
      document.getElementsByClassName("is-size-7-mobile")[1].classList.add("has-background-danger");
      break;
    case 0:
      while(document.getElementsByClassName("has-background-danger").length != 0){
        document.getElementsByClassName("has-background-danger")[0].classList.remove("has-background-danger");
      }
      break;
    default:
      if(row > 4){row += 2}
      document.getElementsByTagName("tr")[row].classList.add("has-background-danger");
      break;
  }
}
