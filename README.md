# M-Cubed-Live

## Installation
The web app is run on a node stack, with a simple server, that is then
containerised/containerized and submitted to the GitLab registry. The latest
version of the container can be found at
`registry.gitlab.com/mraxel/m-cubed-live:latest`. Therefore, if you wish to run
this yourself, a working installation of Docker is required. By default, the
server runs internally on port 8080. This can (obviously) be forwarded to
whichever external port of your choosing.

### Running using `docker-compose`
An example `docker-compose` file is included below. You should modify it to your
needs.
```yaml
m-cubed:
  image: registry.gitlab.com/mraxel/m-cubed-live:latest
  ports:
    - "80:8080"
  environment:
    - "NODE_ENV=production"
```

### Running using `docker run`
```bash
docker run -d -p 80:8080 --name m-cubed -e "NODE_ENV=production" \
       registry.gitlab.com/mraxel/m-cubed-live:latest
```

### A word on security:
This container does not have any sort of SSL/HTTPS encryption enabled or
available. The intended use case is for this to be run behind a reverse-proxy
which will handle the encryption, security and keys. Therefore, we cannot
recommend (and, in fact, strongly advise against) running this verbatim in a
publicly-accessible location.

## What's the story?
A few years back I was frustrated that, as an adjudicator, a lot of simple maths
was required to tally up speaker scores at the end of a debate. They aren't
difficult, it's just there's a lot of them, and a lot of chance for a single
maths error to become a big problem.

My solution? Build an app. I created "M-Cubed" for two reasons: 1) I really
needed an excuse to teach myself how to build an app, and 2) I always have my
phone when I'm adjudicating to keep time, so M-Cubed was born. Essentially a
simple calculator, it presented a nice user interface to allow easy input, had
automatic validation, and presented the results formatted in the same way as
they appear on the scoresheet to allow easy copying into a scoresheet.

Fast forward to know, and lots of people are asking me where they can get this
app. I'd probably be fine forking over the $25 to Google to have this Android
app published on the Google Play Store, however, most (read as: nearly all) of
the people that ask for it are in the iPhone camp. Made of money I am not, so
forking over $99 to Apple to have this made for the App Store, and kept updated
is just out of the question, so instead M-Cubed-Live is a simple hosted web app
that will do basically the same stuff, client side, then I can host the damn
thing and anyone with a recent browser (and a mobile data connection) can use
it independant of what platform they are on.

## A debating calculator?
Yes. This is pre-set with validation for scores as defined by the
_Australia-Asia Debating Guide_. For more information on scoring, please consult
this guide. If you want to change the built-in score validation, feel free to
clone/fork, dive into the source code and have a crack yourself.

## License
See the license file. Basically I don't want this to appear on some app store
for $5 to someone elses name, so GPLv3 it is. Feel free to copy it as much as
you want, host it wherever you want, change it as much as you like. The only
real legal requirment is that you share any changes you make with the rest of
the community by making any changes you make open-source.
