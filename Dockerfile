FROM node:8-alpine

LABEL maintainer="alexander@axel.red" \
      description="A simple companion web-app for adjudicators to calculate debating scores"

WORKDIR /usr/src/app

COPY . .

RUN npm install \
    && npm install -g gulp-cli \
    && gulp

EXPOSE 8080

ENTRYPOINT ["npm", "start"]
